const user_file = require('./user.json');
const cli_file = require('./cliente.json');
const dir_user = './user.json';
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu53db/collections/';

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en ", dir_user);
      }
    })
 };

module.exports = {
  port : process.env.port || 3000,
  URL_BASE : '/techU-peru/v1/',
  URL_MLAB : '/techU-peru/v2/',
  user_file,
  cli_file,
  writeUserDataToFile,
  baseMLabURL
}
