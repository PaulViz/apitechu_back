const global = require('../global');

function login(req,res){
    let tam = global.user_file.length;
    let i = 0;
    let existe = false;
    while(i < tam && !existe){
      if(global.user_file[i].email == req.body.email && global.user_file[i].password == req.body.password){
        existe = true;
        global.user_file[i].logged=true;
        global.writeUserDataToFile(global.user_file);
      }
      i++;
    }
    if (existe) //existe
      res.send({"Login":"Correcto","id : ":i});
    else{
      res.send({"Login":"Incorrecto"});
    }
};
function logout(req,res){
  let tam = global.user_file.length;
  let i = 0;
  let existe = false;
  while(i < tam && !existe){
    if(global.user_file[i].email == req.body.email){
      existe = true;
      delete global.user_file[i].logged;
      global.writeUserDataToFile(global.user_file);
    }
    i++;
  }
  if (existe) //existe
    res.send({"Logout":"Correcto","id : ":i});
  else{
    res.send({"Logout":"Incorrecto"});
  }
};
function userlogin(req,res){
  //console.log(req.body.email);
  let tam = global.user_file.length;
  let i = 0;
  let matriz =  new Array(tam);
  while(i < tam){
    if(global.user_file[i].logged == true){
      console.log(global.user_file[i]);
      matriz.push(global.user_file[i]);
    }
    i++;
  }
  res.send(matriz);
};

module.exports={
  login,
  logout,
  userlogin
};
