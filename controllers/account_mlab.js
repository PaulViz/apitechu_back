const global = require('../global');
var requestJSON = require('request-json');
var apikeyMLab = 'apiKey='+process.env.MLAB_API_KEY;

function getAccountMlab(req, res) {
   console.log(global.baseMLabURL);
   var httpClient = requestJSON.createClient(global.baseMLabURL);
   var id = req.params.id;
   var queryString = 'f={"_id":0}&';
   var queryStringID = 'q={"iduser":' + id + '}&';
   httpClient.get('account?' + queryString + queryStringID + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo cuentas de usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento cuentas de usuario."}
           res.status(404);
         }
       }
       res.send(response);
     });
};

module.exports={
  getAccountMlab
};
