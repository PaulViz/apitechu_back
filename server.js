const express = require('express');
const global = require('./global');
require('dotenv').config('./env');
const user_controller = require('./controllers/user');
const cli_controller = require('./controllers/cliente');
const auth_controller = require('./controllers/auth');
var bodyParser = require('body-parser');
var app = express();

const user_mlab_controller = require('./controllers/user_mlab');
const account_mlab_controller = require('./controllers/account_mlab');
const auth_mlab_controller = require('./controllers/auth_mlab');


//stringify para json
app.use(bodyParser.json());

//======================USUARIOS
//Peticion GET users
app.get(global.URL_BASE + 'users', user_controller.getUsers);
//Peticion GET user con Param ID
app.get(global.URL_BASE + 'users/:id', user_controller.getParamUser);
//Peticion GET user con Query String ID
app.get(global.URL_BASE + 'users/:id', user_controller.getQueryUser);
//Peticion GET clientes
app.get(global.URL_BASE + 'clientes', cli_controller.getClientes);
//POST users Agregar usuario
app.post(global.URL_BASE + 'users', user_controller.addUser);
//PUT users Modificar usuario
app.put(global.URL_BASE + 'users', user_controller.modUser);
//DELETE users Delete usuario
app.delete(global.URL_BASE + 'users/:id', user_controller.delUser);
//=====================AUTENTICACION
//POST Login
app.post(global.URL_BASE + 'login', auth_controller.login);
//POST logout
app.post(global.URL_BASE + 'logout', auth_controller.logout);
//POST GET todos Usuario Logged
app.post(global.URL_BASE + 'userlogin', auth_controller.userlogin);

//GET users consumiendo API REST de mLab
app.get(global.URL_MLAB + 'users', user_mlab_controller.getUserMlab);
//GET account con mLab
app.get(global.URL_MLAB + 'users/:id/accounts', account_mlab_controller.getAccountMlab);
//POST users mLab
app.post(global.URL_MLAB + 'users', user_mlab_controller.addUserMlab);
//PUT user mlab
app.put(global.URL_MLAB + 'users/:id', user_mlab_controller.modUserMlab);

//PUT Login
app.put(global.URL_MLAB + 'login', auth_mlab_controller.loginMlab);
//PUT Logout
app.put(global.URL_MLAB + 'logout', auth_mlab_controller.logoutMlab);

//======================LISTEN
app.listen(global.port, function(){
  console.log('Listen por puerto '+ global.port);
});
