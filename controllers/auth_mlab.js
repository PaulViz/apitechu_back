const global = require('../global');
var requestJSON = require('request-json');
var clienteMlab = requestJSON.createClient(global.baseMLabURL);
//apikeyMLab si se coloca global la variable queda como undefined, al parecer por el .env que se ejecuta luego
var apikeyMLab = 'apiKey='+process.env.MLAB_API_KEY;
var bodyParser = require('body-parser');

function loginMlab(req,res){
  console.log(global.baseMLabURL);
  let email = req.body.email;
  let password = req.body.password;
  //var httpClient = requestJSON.createClient(global.baseMLabURL);
  //Para interpolar expresiones o query templates, al inicio y fin va el acento frances
  var queryString = `q={"email":"${email}","password":"${password}"}&`;
  console.log(queryString);
  //var queryString2 = 'q={"email":"' + email + '","password":"'+ password + '"}&';
  //console.log(queryString2);
  clienteMlab.get('user?' + queryString + apikeyMLab,
    function(err, respuestaMLab, bodyget) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(bodyget.length > 0) {
          let prop_login = '{"$set":{"logged":true}}';
          console.log('bodyput1 '+bodyget[0].first_name);
          console.log(JSON.parse(prop_login));
          clienteMlab.put('user?q={"id":'+bodyget.id+'}&' + apikeyMLab, JSON.parse(prop_login),
          function(errPut, respuestaMLabPut, bodyPut) {
              console.log('bodyput '+bodyget[0].id);
              if(errPut) {
                  response = {"msg" : "Error agregando propiedad logged usuario."}
                  res.status(500);
              }
              else{
                response = {'Login':'Correcto', 'id':bodyget[0].id, 'name':bodyget[0].first_name, 'email':bodyget[0].email,}
                res.status(201).send(response);
              }
          });
          //response = body;
        } else {
          response = {"Login":"Incorrecto"}
          res.status(404).send(response);
        }
      }
    });
};

  function logoutMlab(req,res){
    let email = req.body.email;
    let password = req.body.password;
    //var httpClient = requestJSON.createClient(global.baseMLabURL);
    //Para interpolar expresiones o query templates, al inicio y fin va el acento frances
    var queryString = `q={"email":"${email}","password":"${password}"}&`;
    console.log(queryString);
    //var queryString2 = 'q={"email":"' + email + '","password":"'+ password + '"}&';
    //console.log(queryString2);
    clienteMlab.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, bodyget) {
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(bodyget.length > 0) {
            //let prop_login = '{"$unset":{"logged":""}}';
              let prop_login = '{"$set":{"logged":false}}';
            console.log('prop_login '+prop_login);
            clienteMlab.put('user?q={"id":'+bodyget.id+'}&' + apikeyMLab, JSON.parse(prop_login),
            function(errPut, respuestaMLabPut, bodyPut) {
                console.log('bodyput '+bodyget[0].id);
                if(errPut) {
                    response = {"msg" : "Error quitando propiedad logged usuario."}
                    res.status(500);
                }
                else{
                  response = {'Logout':'Correcto', 'id':bodyget[0].id, 'name':bodyget[0].first_name, 'email':bodyget[0].email,}
                  res.status(201).send(response);
                }
            });
            //response = body;
          } else {
            response = {"Logout":"Incorrecto"}
            res.status(404).send(response);
          }
        }
      });
};

module.exports={
  loginMlab,
  logoutMlab
};
