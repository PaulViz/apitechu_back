const global = require('../global');
var requestJSON = require('request-json');
var clienteMlab = requestJSON.createClient(global.baseMLabURL);
//apikeyMLab si se coloca global la variable queda como undefined, al parecer por el .env que se ejecuta luego
var apikeyMLab = 'apiKey='+process.env.MLAB_API_KEY;

function getUserMlab(req, res) {
   console.log(global.baseMLabURL);
   //var httpClient = requestJSON.createClient(global.baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   clienteMlab.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
};


function addUserMlab(req,res){
    //clienteMlab = requestJSON.createClient(global.baseMLabURL);

    clienteMlab.get('user?' + apikeyMLab,
      function(error, respuestaMLab, body){
        var newID = body.length +1;
        var newUser = {
          "id"          : newID,
          "first_name"  : req.body.first_name,
          "last_name"   : req.body.last_name,
          "email"       : req.body.email,
          "password"    : req.body.password,
        };

        clienteMlab.post('user?' + apikeyMLab, newUser,
          function(error, respuestaMLab, body){
            var response = {};
              console.log(req.body);
            if(error) {
                response = {"msg" : "Error agregando usuario."}
                res.status(500);
            } else {
              response = body;
              res.status(200);
              res.send(body);
              //res.send(response);
            }

          });
        });
    //global.user_file.push(newUser);
    //global.writeUserDataToFile(global.user_file);
    //console.log('Nuevo Usuario : '+ newUser.first_name);
    //res.send(response);
    //res.send({"msg":"POST exitoso"});

};

/*
//PUT users con parámetro 'id'
app.put(URLbase + 'users/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var clienteMlab = requestJSON.createClient(baseMLabURL);
 clienteMlab.get('user?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});
*/
// Petición PUT con id de mLab (_id.$oid)
function modUserMlab(req, res) {

     var id = req.params.id;
     console.log(id);
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     clienteMlab.get('user?' + queryString + apikeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];//Obtiene el primer id si tienes varios repetidos
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         clienteMlab.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 };





module.exports={
  getUserMlab,
  addUserMlab,
  modUserMlab
};
