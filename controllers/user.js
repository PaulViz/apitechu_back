const global = require('../global');

function getParamUser(req, res)
{
    let pos = req.params.id - 1;
    let rpta = (global.user_file [pos] == undefined) ?
       {"msg":"Usuario No Encontrado"} : global.user_file [pos];
    res.send(rpta);
};

function getUsers(req,res) {
    res.send(global.user_file);
};

function getQueryUser(req,res) {
    let pos = req.params.id - 1;
    let tam = user_file.length;
    let rpta = (global.user_file [pos] == undefined) ?
      {"msg":"Usuario No Encontrado!"} : global.user_file [pos];
    res.send(rpta);
};

function addUser(req,res){
    // console.log('POST de users');
    // console.log('Nuevo Usuario : ' + req.body.first_name);
    // console.log('Nuevo Email : ' + req.body.email);
    let newID = global.user_file.length +1;
    let newUser = {
      "id"          : newID,
      "first_name"  : req.body.first_name,
      "last_name"   : req.body.last_name,
      "email"       : req.body.email,
      "password"    : req.body.password,
    };
    global.user_file.push(newUser);
    global.writeUserDataToFile(global.user_file);
    console.log('Nuevo Usuario : '+ newUser.first_name);
    res.send(newUser);
    //res.send({"msg":"POST exitoso"});

};

function modUser(req,res){
    let findID = req.query.id;
    let modifyUser = {
      "ID"          : findID,
      "first_name"  : req.body.first_name,
      "last_name"   : req.body.last_name,
      "email"       : req.body.email,
      "password"    : req.body.password,
    };
    global.user_file[findID-1] = modifyUser;
    global.writeUserDataToFile(global.user_file);
    res.send(modifyUser);
};

function delUser(req,res){
    let findID = req.params.id;
    console.log('findID '+ findID);
    global.user_file.splice(findID-1,1);
    res.send(global.user_file);
};

 module.exports={
   getParamUser,
   getUsers,
   getQueryUser,
   addUser,
   modUser,
   delUser
 };
